<?php if($settings->layout == 'collage') : ?>
.fl-node-<?php echo $id; ?> .fl-mosaicflow {
	margin-left: -<?php echo $settings->photo_spacing; ?>px;
}
.fl-mosaicflow-item {
	margin: 0 0 <?php echo $settings->photo_spacing; ?>px <?php echo $settings->photo_spacing; ?>px;
}
<?php endif; ?>
<?php if( $settings->gallery_size == 'fixed' ) { ?>
.fl-gallery-item {
	display: none;
	height: 150px;
	width: 150px;
}
<?php } else { ?>
.fl-gallery-item {
	display: none;
	height: auto;
	width: 150px;
}

<?php } ?>
