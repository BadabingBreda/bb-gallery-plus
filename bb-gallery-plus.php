<?php
/**
 * Beaver Builder Gallery Plus Plugin
 *
 * @package     KnowTheCode
 * @author      Badabingbreda
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Beaver Builder Gallery Plus Plugin
 * Plugin URI:  https://www.badabing.nl
 * Description: An extended Gallery plugin
 * Version:     1.0.0
 * Author:      Badabingbreda
 * Author URI:  https://www.badabing.nl
 * Text Domain: bbgalleryplus
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

define( 'MY_MODULES_DIR', plugin_dir_path( __FILE__ ) );
define( 'MY_MODULES_URL', plugins_url( '/', __FILE__ ) );

function my_load_module_examples() {
    if ( class_exists( 'FLBuilder' ) ) {
        // Include your custom modules here.
        require_once ('gallery-plus/gallery-plus.php');
    }
}
add_action( 'init', 'my_load_module_examples' );


