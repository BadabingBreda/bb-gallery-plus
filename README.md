<<<<<<< HEAD
=======
## Beaver Builder Gallery Plus ##

Wordpress Plugin for Beaver Builder which enables user to add gallery module and select custom size image for thumbnail.

This module features two extra settings, 'Thumbnail Size' and 'Gallery Grid'

Setting the '**Thumbnail Size**' enables you to use theme-specific image-dimensions that are defined using the add_image_size function.

Setting the '**Gallery Grid**' to 'match' will enable the grid to resize to something other than 1:1 proportions, but still maintaining a grid layout by matching all items to the same height, even if they really aren't.

! CAUTION

Please be aware that custom image sizes are either generated on upload or by third party plugins like '*regenerate thumbnails*'. You should check if you need to regenerate whenever you change your theme. Also, if certain minimum sizes aren't met a thumbnail will not be created. Selecting this size in the module might result in unexpected behavior.
>>>>>>> ba14b10fd66709fba776ea7ec7f279074313bee3
